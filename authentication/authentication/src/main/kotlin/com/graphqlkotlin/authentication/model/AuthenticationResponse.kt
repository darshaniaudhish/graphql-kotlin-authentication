package com.graphqlkotlin.authentication.model

import com.expediagroup.graphql.generator.annotations.GraphQLDescription
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class AuthenticationResponse
@JsonCreator constructor(
        @GraphQLDescription(value = "If the supplied userName and corresponding credential is correct then this boolean is set true, otherwise false.")
        @JsonProperty("isAuthenticated") val isAuthenticated: Boolean,

        @GraphQLDescription(value = "Time(in epoch) when this token will expire.")
        @JsonProperty("expiresAt") val expiresAt: Long?,

        @GraphQLDescription(value = "If the supplied userName and corresponding credential is correct then the system will return a JWT token, otherwise empty string.")
        @JsonProperty("accessToken") val accessToken: String = ""
) {

}