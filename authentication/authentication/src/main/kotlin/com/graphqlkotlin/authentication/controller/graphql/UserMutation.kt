package com.graphqlkotlin.authentication.controller.graphql

import com.expediagroup.graphql.generator.annotations.GraphQLDescription
import com.expediagroup.graphql.server.operations.Mutation
import com.graphqlkotlin.authentication.model.AuthenticationResponse
import com.graphqlkotlin.authentication.service.UserService
import org.springframework.stereotype.Component

@Component
class UserMutation(
        private val userService: UserService
) : Mutation {
    @GraphQLDescription(value = "This mutation allows a user to authenticate themselves and retrieve an authorization token.")
    suspend fun authenticateUser(

            @GraphQLDescription(value = "The supplied value of this parameter is used to find if a user is registered  in the system.")
            userName: String,

            @GraphQLDescription(value = "This parameter is the user's authentication string which will allow login if validated by the backend.")
            password: String
    ): AuthenticationResponse? = userService.authenticate(userName, password)
}