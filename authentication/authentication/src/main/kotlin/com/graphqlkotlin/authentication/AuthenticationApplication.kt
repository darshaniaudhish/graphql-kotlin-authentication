package com.graphqlkotlin.authentication

import com.graphqlkotlin.authentication.initializer.DataLoader
import com.graphqlkotlin.authentication.initializer.loadData
import com.graphqlkotlin.authentication.model.UserDocument
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import reactor.core.publisher.Flux
import java.io.InputStream

@SpringBootApplication
class AuthenticationApplication

fun main(args: Array<String>) {

    runApplication<AuthenticationApplication>(*args) {

        val bufferedInputStream: InputStream? =
                this.classLoader.getResourceAsStream("users.json")
        val content: String = bufferedInputStream?.let { DataLoader.getResource(it) } as String
        val contentList: Flux<UserDocument>? = DataLoader.convertToList(content)
        addInitializers(loadData(contentList))
    }
}

