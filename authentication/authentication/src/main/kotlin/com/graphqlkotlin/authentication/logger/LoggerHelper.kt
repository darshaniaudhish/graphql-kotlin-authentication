package com.graphqlkotlin.authentication.logger

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class LoggerHelper private constructor() {
    companion object {
        const val LOG_ERRROR = "Error"
        const val LOG_INFO = "Info"
        const val LOG_TRACE = "Trace"
        const val LOG_DEBUG = "Debug"
        fun getLogger(className: Class<*>?): Logger {
            return LoggerFactory.getLogger(className)
        }

        fun log(logger: Logger, format: String?, vararg arguments: Any?) {
            log(LOG_ERRROR, logger, format, *arguments)
        }

        fun log(logLevel: String?, logger: Logger, format: String?, vararg arguments: Any?) {
            when (logLevel) {
                LOG_ERRROR -> logger.error(format, *arguments)
                LOG_INFO -> logger.info(format, *arguments)
                LOG_DEBUG -> logger.debug(format, *arguments)
                LOG_TRACE -> logger.trace(format, *arguments)
                else -> logger.info(format, *arguments)
            }
        }

        fun log(logLevel: String?, logger: Logger, message: String?) {
            when (logLevel) {
                LOG_ERRROR -> logger.error(message)
                LOG_INFO -> logger.info(message)
                LOG_DEBUG -> logger.debug(message)
                LOG_TRACE -> logger.trace(message)
                else -> logger.info(message)
            }
        }

        fun logError(logger: Logger, message: String?) {
            log(LOG_ERRROR, logger, message)
        }

        fun logInfo(logger: Logger, message: String?) {
            log(LOG_INFO, logger, message)
        }

        fun logDebug(logger: Logger, message: String?) {
            log(LOG_DEBUG, logger, message)
        }

        fun logConstructorEntry(logger: Logger, className: String?) {
            logger.trace("Class '{}' instance instantiation started", className)
        }

        fun logConstructorExit(logger: Logger, className: String?) {
            logger.trace("Class '{}' instance instantiated", className)
        }

        fun logMethodEntry(logger: Logger, className: String?, methodName: String?) {
            logger.info("Class '{}' method '{}' invoked", className, methodName)
        }

        fun logMethodExit(logger: Logger, className: String?, methodName: String?) {
            logger.info("Class '{}' method '{}' ended", className, methodName)
        }

        fun logExceptionWithMessage(logger: Logger, message: String?, e: Exception) {
            var message = message
            if (message == null || message.isEmpty()) {
                message = "Exception occurred"
            }
            message = restrictLogMessageLength(message)
            val logMessage = "$message - '{}' - '{}'"
            val exceptionMessage = restrictLogMessageLength(e.message!!)
            logger.error(logMessage, e.javaClass.getName(), exceptionMessage)
            logger.trace(logMessage, e.javaClass.getName(), exceptionMessage, e)
            logger.debug(logMessage, e.javaClass.getName(), exceptionMessage, e)
        }

        fun restrictLogMessageLength(logMessage: String): String {
            restrictLogMessageLength(logMessage, 1024)
            return logMessage
        }

        fun restrictLogMessageLength(logMessage: String?, logMessageLength: Int): String? {
            var logMessage = logMessage
            if (logMessage != null) {
                validateLogMessage(logMessage)
                if (logMessage.length > logMessageLength) {
                    logMessage = logMessage.substring(0, logMessageLength)
                }
            }
            return logMessage
        }

        fun validateLogMessage(logMessage: String?): String? {
            var logMessage = logMessage
            if (logMessage != null) {
                logMessage = logMessage.replace("[\\n\\t ]".toRegex(), "")
            }
            return logMessage
        }
    }

    init {
        throw IllegalStateException("LoggerHelper")
    }
}