package com.graphqlkotlin.authentication.service

import com.graphqlkotlin.authentication.logger.LoggerHelper
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.util.*
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class AuthenticationService {

    private val logger: Logger = LoggerHelper.getLogger(AuthenticationService::class.java)

    @Value("\${authServer.appSecret}")
    private val appSecret:String =""

    fun getAuthToken(userName: String, email: String): String {
        LoggerHelper.logMethodEntry(logger,"AuthenticationService","getAuthToken")

        val claims: HashMap<String, Any?> = HashMap<String, Any?>();
        claims.put("issuer", "GraphQL-Kotlin Authentication Server");
        claims.put("subject", "AccessToken");
        claims.put("userName", userName);
        claims.put("email", email);
        claims.put("generatedTimestamp", Date().time);

        val expiryDate: Date = DateUtils.addHours(Date(), 1);

        // make a jwt out of the username, email and current time
        val accessToken: String = Jwts.builder()
                .setClaims(claims)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS256, appSecret)
                .compact();

        LoggerHelper.logMethodExit(logger,"AuthenticationService","getAuthToken")
        return accessToken
    }
}