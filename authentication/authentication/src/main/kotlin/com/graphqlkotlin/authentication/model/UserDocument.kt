package com.graphqlkotlin.authentication.model

import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "User")
data class UserDocument(
        var firstName: String,
        var lastName: String,
        var userName: String,
        var email: String,
        var password: String
)