package com.graphqlkotlin.authentication.controller.graphql

import com.expediagroup.graphql.generator.annotations.GraphQLDescription
import com.graphqlkotlin.authentication.model.User
import com.graphqlkotlin.authentication.service.UserService
import org.springframework.stereotype.Component

@Component
class UserQuery(
        private val userService: UserService
) : com.expediagroup.graphql.server.operations.Query {

    @GraphQLDescription(value = "This query allows administrators to retrieve all users' information registered in the system based on the supplied parameters. At least one parameter is mandatory.")
    suspend fun user(
            @GraphQLDescription(value = "The value of this parameter is used to find users whose userName starts with the supplied value")
            usernameStartsWith: String? = ""
    ): List<User>? = userService.getUsers(usernameStartsWith)

}