package com.graphqlkotlin.authentication.initializer

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.graphqlkotlin.authentication.logger.LoggerHelper
import com.graphqlkotlin.authentication.model.UserDocument
import com.graphqlkotlin.authentication.repository.UserRepository
import com.graphqlkotlin.authentication.service.AuthenticationService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.context.support.beans
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.io.BufferedReader
import java.io.InputStream
import java.util.*

@Service
class DataLoader {
    private val logger: Logger = LoggerHelper.getLogger(AuthenticationService::class.java)
    companion object {
        fun convertToList(content: String): Flux<UserDocument>? {
            val mapper = jacksonObjectMapper()
            val contentList: ArrayList<UserDocument> = mapper.readValue(content)
            var users: Flux<UserDocument>? = Flux.fromIterable(contentList)
            return users
        }

        fun getResource(
                bufferedInputStream: InputStream
        ): String {

            val reader = BufferedReader(bufferedInputStream.reader())
            val content = StringBuilder()
            try {
                var line = reader.readLine()
                while (line != null) {
                    content.append(line)
                    line = reader.readLine()
                }
            } finally {
                reader.close()
            }
            return content.toString()
        }
    }
}

fun loadData(content: Flux<UserDocument>?) = beans {
    bean {
        CommandLineRunner {
            LoggerHelper.logMethodEntry(LoggerFactory.getLogger("loadData"),"DataLoader","loadData")
            if (content != null) {

                ref<UserRepository>().deleteAll()
                        .thenMany(content.flatMap(ref<UserRepository>()::save))
                        .subscribe(System.out::println)
            }
            LoggerHelper.logMethodEntry(LoggerFactory.getLogger("loadData"),"DataLoader","loadData")
        }
    }
}