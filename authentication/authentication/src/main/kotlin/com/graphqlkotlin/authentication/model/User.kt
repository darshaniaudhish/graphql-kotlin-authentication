package com.graphqlkotlin.authentication.model

import com.expediagroup.graphql.generator.annotations.GraphQLDescription

data class User(
        @GraphQLDescription(value = "If this parameter is present in the query, the selected users' corresponding attribute will be fetched")
        val firstName: String,

        @GraphQLDescription(value = "If this parameter is present in the query, the selected users' corresponding attribute will be fetched")
        val lastName: String,

        @GraphQLDescription(value = "If this parameter is present in the query, the selected users' corresponding attribute will be fetched")
        val userName: String,

        @GraphQLDescription(value = "If this parameter is present in the query, the selected users' corresponding attribute will be fetched")
        val email: String
)

