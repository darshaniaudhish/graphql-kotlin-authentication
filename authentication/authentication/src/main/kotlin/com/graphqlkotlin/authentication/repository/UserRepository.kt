package com.graphqlkotlin.authentication.repository

import com.graphqlkotlin.authentication.model.UserDocument
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
interface UserRepository : ReactiveMongoRepository<UserDocument, String> {

    @Query("{userName: ?0, password : ?1}")
    fun findByUserNameAndPassword(userName: String, password: String): Mono<UserDocument>?
}