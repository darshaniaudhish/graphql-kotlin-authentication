package com.graphqlkotlin.authentication.service

import com.graphqlkotlin.authentication.logger.LoggerHelper
import com.graphqlkotlin.authentication.model.AuthenticationResponse
import com.graphqlkotlin.authentication.model.User
import com.graphqlkotlin.authentication.model.UserDocument
import com.graphqlkotlin.authentication.repository.UserRepository
import kotlinx.coroutines.reactive.awaitFirstOrDefault
import kotlinx.coroutines.reactive.awaitFirstOrElse
import org.slf4j.Logger
import org.springframework.boot.context.properties.bind.Bindable.listOf
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*

@Service
class UserService(
        private val userRepository: UserRepository,
        private val authenticationService: AuthenticationService
) {
    private val logger:Logger = LoggerHelper.getLogger(UserService::class.java)

    suspend fun getUsers(
            usernameStartsWith: String?
    ): List<User> {
        LoggerHelper.logMethodEntry(logger,"UserService","getUsers")

        var users: MutableList<User> = ArrayList()
        val userDocs: ArrayList<UserDocument> = userRepository.findAll().collectList().awaitFirstOrDefault(listOf()) as ArrayList<UserDocument>;
        for (doc in userDocs) {
            var user: User = User(doc.firstName, doc.lastName, doc.userName, doc.email)
            users.add(user)
        }
        if(usernameStartsWith != null && usernameStartsWith?.length>0) {
            var removableUsers: MutableList<User> = ArrayList()
            for (user in users) {
                if (!user.userName.startsWith(usernameStartsWith)) {
                    removableUsers.add(user)
                }
            }
            users.removeAll(removableUsers)
        }

        LoggerHelper.logMethodExit(logger,"UserService","getUsers")
        return users
    }

    suspend fun authenticate(userName: String, password: String): AuthenticationResponse? {
        LoggerHelper.logMethodEntry(logger,"UserService","authenticate")
        val value: UserDocument? = userRepository.findByUserNameAndPassword(userName, password)
                ?.awaitFirstOrElse { UserDocument("", "", "", "", "") }
        var response: AuthenticationResponse? = null
        if (value != null) {
            if (value.firstName.equals("")) {
                response = AuthenticationResponse(false, null, "")
            } else {
                var accessToken = authenticationService.getAuthToken(value.userName, value.email)
                response = AuthenticationResponse(true, Instant.now().epochSecond + 3600, accessToken)
            }
        }
        LoggerHelper.logMethodExit(logger,"UserService","authenticate")
        return response;
    }
}