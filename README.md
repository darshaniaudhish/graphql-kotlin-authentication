# graphql-kotlin-authentication

This application implements GraphQL Query and Mutation using [graphql-kotlin](https://github.com/ExpediaGroup/graphql-kotlin) library.
Following are the graphql endpoints that are implemented. 

1. User (Query to retrieve all users in the database)
2. authenticateUser (Mutation to authenticate the user and provide an authentication token)

Complete documentation of these endpoints is served by the swagger exposed at endpoint `/playground`

## Run

### Pre-requisites:

1. Docker 

The application has been containerized and you simply need Docker-compose.yml file to run this suite.

```
wget https://gitlab.com/darshaniaudhish/graphql-kotlin-authentication/-/raw/main/authentication/authentication/Docker-compose.yml && docker-compose -f Docker-compose.yml up -d
```

The `wget` command will download the yaml file from the Gitlab, which is required for Docker to run this application. If you are on Windows, kindly download this file manually and place it in a directory of your choice before running the command.  

The `docker-compose` command uses the aforementioned files and starts the following containers in detached(-d) mode. 
- _mongo:latest_ 

This is the official container for Mongodb
- _darshaniaudhish/graphql-kotlin-auth_

This is the container for this application built on top of the official _openjdk_ container. It contains a precompiled jar of this application in the working directory `/authentication`.
If you want to build the jar yourself, kindly refer to the Development section below.


Please ensure that port 8080 is available (not bound to any other application) on the localhost before running the above command.

The application is accessible via GraphQL playground in the browser from the URL `http://localhost:8080/playground`.


## Development

### Pre-requisites: 
1. JAVA version 1.8
2. Apache Maven
3. Docker
a. Container of _mongo:latest_
4. Eclipse or any other JAVA IDE

### Setup:

```
git clone https://gitlab.com/darshaniaudhish/graphql-kotlin-authentication.git

git checkout main

```

Build the project using maven. The jar file should be available at `/authentication/target`.
Import the directory as a maven project in your IDE, use env variable `spring.profiles.active=local` and you are good to proceed forward with the development.
